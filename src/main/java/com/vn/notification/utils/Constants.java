package com.vn.notification.utils;

/**
 * @author LiND
 */
public class Constants {
    public final static String DEFAULT_FULL_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static interface MESSAGE_STATUS {
        public static final String CELL_6 = "Giá đặt";
        public static final String CELL_7 = "Khối lượng đặt";
    }
}
