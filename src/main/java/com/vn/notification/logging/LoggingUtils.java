package com.vn.notification.logging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;

/**
 * @author LiND
 */
public class LoggingUtils {
	public static String objToStringIgnoreEx(Object prm) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS , false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		try {
			return mapper.writeValueAsString(prm);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}
}
