package com.vn.notification.logging;

import org.aspectj.lang.JoinPoint;

/**
 * @author LiND
 */
public interface AutoLoggingService {
	public void writeAccessLog(JoinPoint joinPoint);
}
