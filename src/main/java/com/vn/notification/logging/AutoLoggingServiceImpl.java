package com.vn.notification.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author LiND
 */
@Service
public class AutoLoggingServiceImpl implements AutoLoggingService {
	private final Logger log = LoggerFactory.getLogger(AutoLoggingServiceImpl.class);
	private final String[] IP_HEADER_CANDIDATES = { "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
			"HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP",
			"HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR" };

	@Async
	@Override
	public void writeAccessLog(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		RestController controller = method.getAnnotation(RestController.class);
		RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
		Object[] args = joinPoint.getArgs();
		List<Object> argsForLogging = new ArrayList<Object>();

		HttpServletRequest request = null;
		Map<String, Object> userAccessLog = new HashMap<String, Object>();
		for (Object arg : args) {
			if (arg instanceof HttpServletRequest) {
				request = (HttpServletRequest) arg;
			} else {
				argsForLogging.add(arg);
			}
		}

		userAccessLog.put("ip", remoteIp(request));
		userAccessLog.put("userAgent", userAgent(request));
		userAccessLog.put("path", AnnotationUtils.getValue(requestMapping, "value"));
		userAccessLog.put("time", System.currentTimeMillis());
		log.debug("logBefore - User Access log:{}", LoggingUtils.objToStringIgnoreEx(userAccessLog));
	}

	private String remoteIp(HttpServletRequest request) {
		if (request != null) {
			for (String header : IP_HEADER_CANDIDATES) {
				String ip = request.getHeader(header);
				if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
					return ip;
				}
			}
			return request.getRemoteAddr();
		} else {
			return "";
		}
	}

	private String userAgent(HttpServletRequest request) {
		if (request != null) {
			String userAgent = request.getHeader("User-Agent");
			return userAgent;
		} else {
			return "";
		}
	}
}
