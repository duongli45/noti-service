package com.vn.notification.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author LiND
 */
@Aspect
@Component
public class LoggingControllerHandler {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AutoLoggingService autoLoggingService;

	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
	public void controller() {
	}

	@Pointcut("execution(public * *(..))")
	protected void allMethod() {
	}

	@Before("controller() && allMethod() && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void logBefore(JoinPoint joinPoint) {
		autoLoggingService.writeAccessLog(joinPoint);
	}

	@AfterReturning(pointcut = "controller() && allMethod()", returning = "result")
	public void logAfter(JoinPoint joinPoint, Object result) {
		if (result instanceof Map || result instanceof List) {
		} else {
			log.debug("logAfter - result:{}", LoggingUtils.objToStringIgnoreEx(result));
		}
	}

	@AfterThrowing(pointcut = "controller() && allMethod()", throwing = "exception")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable exception) {
		log.error("An exception has been thrown in " + joinPoint.getSignature().getName() + " ()");
		log.error("Cause : " + exception.getCause());
		log.error(exception.getMessage(), exception);
	}
}