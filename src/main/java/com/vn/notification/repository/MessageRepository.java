package com.vn.notification.repository;

import com.vn.notification.entity.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MessageRepository extends MongoRepository<Message, String> {
    Optional<List<Message>> findAllByStatus(String status);

    @Query("{'Status' : {'$ne' : ?0}}")
    Optional<List<Message>> findAllByStatusNotLike(String x);
}