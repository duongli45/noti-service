package com.vn.notification.repository;

import com.vn.notification.entity.MessageType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MessageTypeRepository extends MongoRepository<MessageType, String> {
    Optional<MessageType> findByCode(String code);
}