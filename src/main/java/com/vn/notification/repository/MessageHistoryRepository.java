package com.vn.notification.repository;

import com.vn.notification.entity.MessageHistory;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageHistoryRepository extends MongoRepository<MessageHistory, String> {
}