package com.vn.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EntityScan(basePackages = "com.vn.notification.entity")
@EnableMongoRepositories
@EnableScheduling
@EnableCaching
public class IialbumApplication {

	public static void main(String[] args) {
		SpringApplication.run(IialbumApplication.class, args);
	}

}
