package com.vn.notification.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author LiND
 */
@Data
@Builder
@Document(collection = "dicMessageTypes")
public class MessageType {
    @Id
    private ObjectId _id;

    @Field(value = "Code")
    private String code;

    @Field(value = "Name")
    private String name;

    @Field(value = "Priority")
    private String priority;
}
