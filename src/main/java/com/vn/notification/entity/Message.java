package com.vn.notification.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author LiND
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "sysMessages")
public class Message  implements Serializable {
    private static final long serialVersionUID = 2048542510306483637L;

    @Id
    private ObjectId _id;

    @Field(value = "Phone")
    private String phone;

    @Field(value = "Content")
    private String content;

    @Field(value = "InTime")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date intime;

    @Field(value = "LastUpdateTime")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date lastUpdateTime;

    @Field(value = "Status")
    private String status;

    @Field(value = "Type")
    private String type;

    @Field(value = "MessageCode")
    private String messageCode;

    @Field(value = "Data")
    private String data;
}
