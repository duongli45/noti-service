package com.vn.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LiND
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SMSRespDataDto implements Serializable {
    private static final long serialVersionUID = 5867771639956325171L;

    private String Brandname;
    private String Phonenumber;
    private String Message;
    private String Status;
    private String StatusDescription;
}
