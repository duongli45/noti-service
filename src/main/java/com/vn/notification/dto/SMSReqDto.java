package com.vn.notification.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LiND
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SMSReqDto implements Serializable {
    private static final long serialVersionUID = -2219291827401112103L;

    private String Brandname;
    private String Message;
    private String Phonenumber;
    private String user;
    private String pass;
}
