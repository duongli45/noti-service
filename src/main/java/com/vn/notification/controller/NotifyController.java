package com.vn.notification.controller;

import com.vn.notification.dto.SMSReqDto;
import com.vn.notification.logging.LoggingUtils;
import com.vn.notification.service.impl.NotifyServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/")
@Tag(name = "sms", description = "SMS API")
public class NotifyController {
    private final Logger logger = LoggerFactory.getLogger(NotifyController.class);

    @Operation(summary = "Send single SMS", description = "Send single SMS", tags = { "sms" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Send SMS successfully"),
            @ApiResponse(responseCode = "401", description = "You are not authorized to view the resource"),
            @ApiResponse(responseCode = "403", description = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(responseCode = "500", description = "Internal Server error"), })
    @GetMapping(value = "/sms", produces = { "application/json", "application/xml" })
    public ResponseEntity<?> sendSingleSMS(HttpServletRequest request, @RequestBody SMSReqDto dto) {
        logger.info("sendSingleSMS - data: {}",   LoggingUtils.objToStringIgnoreEx(dto));

        return ResponseEntity.ok().build();
    }
}
