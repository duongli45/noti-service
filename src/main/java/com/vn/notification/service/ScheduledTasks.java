package com.vn.notification.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author LiND
 */

@Component
public class ScheduledTasks {
    private final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    private NotifyService notifyService;

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Scheduled(fixedRateString = "${sms.fixedRate}")
    public void scheduleTaskSendSMS() {
        logger.info("scheduleTaskSendSMS :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()) );
        notifyService.scheduleSendSMS();
    }

    @Scheduled(cron = "${sms.cron}")
    public void scheduleTaskBackupSMS() {
        logger.info("scheduleTaskBackupSMS :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()) );
        notifyService.backupSMS();
    }
}
