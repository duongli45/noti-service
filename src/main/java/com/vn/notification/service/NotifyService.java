package com.vn.notification.service;

import com.vn.notification.dto.SMSReqDto;
import com.vn.notification.dto.SMSRespDto;

import java.util.Optional;

public interface NotifyService {
    Optional<SMSRespDto> sendSMS(SMSReqDto dto);
    void scheduleSendSMS();
    void backupSMS();
}
