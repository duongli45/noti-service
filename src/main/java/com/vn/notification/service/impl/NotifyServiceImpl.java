package com.vn.notification.service.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vn.notification.dto.MessageDto;
import com.vn.notification.dto.SMSReqDto;
import com.vn.notification.dto.SMSRespDto;
import com.vn.notification.entity.Message;
import com.vn.notification.entity.MessageHistory;
import com.vn.notification.entity.MessageType;
import com.vn.notification.logging.LoggingUtils;
import com.vn.notification.repository.MessageHistoryRepository;
import com.vn.notification.repository.MessageRepository;
import com.vn.notification.repository.MessageTypeRepository;
import com.vn.notification.service.NotifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author LiND
 */
@Service
public class NotifyServiceImpl implements NotifyService {
    private final Logger logger = LoggerFactory.getLogger(NotifyServiceImpl.class);

    @Value("${sms.url.single}")
    private String singleSMSUrl;
    @Value("${sms.url.batch}")
    private String batchSMSUrl;
    @Value("${sms.brandname}")
    private String smsBrandName;
    @Value("${sms.username}")
    private String smsUserName;
    @Value("${sms.password}")
    private String smsPassword;
    @Value("${sms.limit}")
    private int limit;

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageTypeRepository messageTypeRepository;
    @Autowired
    private MessageHistoryRepository messageHistoryRepository;

    @Autowired
    private RestTemplate restTemplate;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

    @Override
    public Optional<SMSRespDto> sendSMS(SMSReqDto dto) {
        String url = singleSMSUrl;
        dto.setBrandname(smsBrandName);
        dto.setUser(smsUserName);
        dto.setPass(smsPassword);
        logger.info("sendSMS - url: {} - request: {}", url, LoggingUtils.objToStringIgnoreEx(dto));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<SMSReqDto> httpEntity = new HttpEntity<>(dto, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                url,
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        Optional<SMSRespDto> dtoOptional = Optional.empty();
        if (response.getStatusCode() != HttpStatus.OK) {
            logger.error("sendSMS - url: {} - response: {}", url, LoggingUtils.objToStringIgnoreEx(response.getBody()));
        } else {
            try {
                JsonObject jsonObject = new JsonParser().parse(response.getBody()).getAsJsonObject();
                dtoOptional = Optional.ofNullable(SMSRespDto.builder()
                        .Code(jsonObject.get("Code").getAsString())
                        .Description(jsonObject.get("Description").getAsString())
                        .Data("null".equalsIgnoreCase(String.valueOf(jsonObject.get("Data")))? null:jsonObject.getAsJsonObject("Data").toString())
                        .build());
                logger.info("sendSMS - url: {} - response: {}", url, dtoOptional.get());
            } catch (Exception e) {
                logger.error("sendSMS - url: {} - request: {} - error: {}",
                        url, LoggingUtils.objToStringIgnoreEx(dto), e.getMessage());
            }
        }
        return dtoOptional;
    }

    @Override
    public void scheduleSendSMS() {
        Optional<List<Message>> allByStatus = messageRepository.findAllByStatus("0");
        logger.info("allByStatus - size: {} - data: {}", allByStatus.get().size(), LoggingUtils.objToStringIgnoreEx(allByStatus.get()));

        if (allByStatus.isPresent()) {
            List<MessageDto> messageDtos = allByStatus.get()
                    .stream()
                    .map(t -> getPriority(t.getType(), t))
                    .sorted(Comparator.comparingInt(MessageDto::getPriority)
                                        .thenComparing(MessageDto::getIntime))
                    .limit(limit)
                    .collect(Collectors.toList());
            logger.info("allByStatus sorted: {}", LoggingUtils.objToStringIgnoreEx(messageDtos));

            List<Message> list = new ArrayList<>();
            for (MessageDto t : messageDtos) {
                SMSReqDto smsReqDto = SMSReqDto.builder()
                        .Phonenumber(t.getPhone())
                        .Message(t.getContent())
                        .build();
                Optional<SMSRespDto> smsRespDto = sendSMS(smsReqDto);
                if (smsRespDto.isPresent()) {
                    t.setStatus(smsRespDto.get().getCode());
                    t.setData(smsRespDto.get().getData());
                    t.setLastUpdateTime(new Timestamp(System.currentTimeMillis()));
                }
                Message message = new Message();
                BeanUtils.copyProperties(t, message);
                list.add(message);
            }

            messageRepository.saveAll(list);
        }
    }

    @Override
    public void backupSMS() {
        Optional<List<Message>> messages = messageRepository.findAllByStatusNotLike("0");
        if (messages.isPresent()) {
            List<MessageHistory> messageHistories = messages.get()
                    .stream()
                    .map(t -> toDto(t))
                    .collect(Collectors.toList());

            messageHistoryRepository.saveAll(messageHistories);
        }
    }

    private MessageHistory toDto(Message dto) {
        MessageHistory t = new MessageHistory();
        BeanUtils.copyProperties(dto, t);
        return t;
    }

    private MessageDto getPriority(String code, Message message) {
        MessageDto t = new MessageDto();
        BeanUtils.copyProperties(message, t);
        Optional<MessageType> messageType = messageTypeRepository.findByCode(code);
        messageType.ifPresent(type -> t.setPriority(Integer.parseInt(type.getPriority())));
        return t;
    }
}
