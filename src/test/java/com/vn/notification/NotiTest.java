package com.vn.notification;

import com.vn.notification.dto.SMSReqDto;
import com.vn.notification.dto.SMSRespDto;
import com.vn.notification.entity.Message;
import com.vn.notification.entity.MessageType;
import com.vn.notification.logging.LoggingUtils;
import com.vn.notification.repository.MessageRepository;
import com.vn.notification.repository.MessageTypeRepository;
import com.vn.notification.service.NotifyService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

/**
 * @author LiND
 */
@ContextConfiguration(classes = IialbumApplication.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotiTest {
    @Autowired
    private NotifyService notifyService;

    @Autowired
    private MessageTypeRepository messageTypeRepository;
    @Autowired
    private MessageRepository messageRepository;

    @Test
    @Ignore
    public void sms() {
        SMSReqDto a = SMSReqDto.builder()
                .Brandname("Li")
                .Message("test ne")
                .Phonenumber("0782820171")
                .user("testcmc")
                .pass("BtBt8e2d")
                .build();
        Optional<SMSRespDto> smsRespDto = notifyService.sendSMS(a);
        System.out.println("smsRespDto = " + smsRespDto);
    }

    @Test
    @Ignore
    public void sms1() throws ParseException {
        notifyService.scheduleSendSMS();
    }

    @Test
    @Ignore
    public void sms2() {
        List<MessageType> all = messageTypeRepository.findAll();
        System.out.println("LoggingUtils.objToStringIgnoreEx(all) = " + LoggingUtils.objToStringIgnoreEx(all));
    }

    @Test
    @Ignore
    public void sms3() {
        Optional<List<Message>> allByStatusIsNot = messageRepository.findAllByStatusNotLike("0");
        int size = allByStatusIsNot.get().size();
        System.out.println("size = " + size);
        System.out.println("LoggingUtils.objToStringIgnoreEx(allByStatusIsNot.get()) = " + LoggingUtils.objToStringIgnoreEx(allByStatusIsNot.get()));
    }
}
